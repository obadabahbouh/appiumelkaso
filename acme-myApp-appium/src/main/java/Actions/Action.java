package Actions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.ImmutableMap;

import Elkaso.base.Base;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Action extends Base {

	public void waitforseconds(long seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}

	public void waitForElementToLoad(AndroidDriver<MobileElement> driver, WebElement element) {
		waitforseconds(3);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void click(MobileElement element) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	public void scrollAndClick(String visibleText) {
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""
						+ visibleText + "\").instance(0))")
				.click();
	}

	public void hideKeyboard() {

		driver.hideKeyboard();

	}

	public void backButton() {
		driver.navigate().back();
	}

	public void EnterValue(String MobileNumber) {
		try {
			List<String> addMobileNumberArg = Arrays.asList(MobileNumber);
			Map<String, Object> addMobileNumberCmd = ImmutableMap.of("command", "input text", "args",
					addMobileNumberArg);
			driver.executeScript("mobile: shell", addMobileNumberCmd);

		} catch (WebDriverException e) {
			System.out.println("An exceptional case.");
		}
	}

}
