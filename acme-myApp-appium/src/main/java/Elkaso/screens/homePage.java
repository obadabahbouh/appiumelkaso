package Elkaso.screens;

import Elkaso.base.Base;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class homePage extends Base{
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Chats Chats Tab 1 of 4")
	public MobileElement ChatButton;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Suppliers Suppliers Tab 2 of 4")
	public MobileElement SuppliersButton;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Track Track Tab 3 of 4")
	public MobileElement TrackButton;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Settings Settings Tab 4 of 4")
	public MobileElement SettingsButton;

}
