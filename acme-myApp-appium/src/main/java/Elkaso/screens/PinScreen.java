package Elkaso.screens;

import Elkaso.base.Base;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class PinScreen extends Base{
	
	@iOSXCUITFindBy (xpath = "//XCUIElementTypeApplication[@name=\"Elkaso\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]")
	@AndroidFindBy (className = "android.view.View")
	public MobileElement EnterPinCode;

	@iOSXCUITFindBy (accessibility = "Verify")
	@AndroidFindBy (accessibility ="Verify")
	public MobileElement verifyButton;
	
	@iOSXCUITFindBy (accessibility = "0")
	@AndroidFindBy (accessibility = "0")
	public MobileElement Num0;
	
	@iOSXCUITFindBy (accessibility = "1")
	@AndroidFindBy (accessibility = "1")
	public MobileElement Num1;
	
	@iOSXCUITFindBy (accessibility = "2")
	@AndroidFindBy (accessibility = "2")
	public MobileElement Num2;
	
	@iOSXCUITFindBy (accessibility = "3")
	@AndroidFindBy (accessibility = "3")
	public MobileElement Num3;
	
	@iOSXCUITFindBy (accessibility = "4")
	@AndroidFindBy (accessibility = "4")
	public MobileElement Num4;
	
	@iOSXCUITFindBy (accessibility = "5")
	@AndroidFindBy (accessibility = "5")
	public MobileElement Num5;
	
	@iOSXCUITFindBy (accessibility = "6")
	@AndroidFindBy (accessibility = "6")
	public MobileElement Num6;
	
	@iOSXCUITFindBy (accessibility = "7")
	@AndroidFindBy (accessibility = "7")
	public MobileElement Num7;
	
	@iOSXCUITFindBy (accessibility = "8")
	@AndroidFindBy (accessibility = "8")
	public MobileElement Num8;
	
	@iOSXCUITFindBy (accessibility = "9")
	@AndroidFindBy (accessibility = "9")
	public MobileElement Num9;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility = "Settings\nSettings\nTab 4 of 4")
	public MobileElement SettingsButton;
	
//	@iOSXCUITFindBy (accessibility = "")
//	@AndroidFindBy (uiAutomator="+ +9715443
//	public MobileElement profileMobileNumber;
	

}
