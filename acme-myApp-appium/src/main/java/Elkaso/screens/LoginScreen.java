package Elkaso.screens;


import java.util.List;

import Elkaso.base.Base;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;


public class LoginScreen extends Base {

	@iOSXCUITFindBy (accessibility = "GET STARTED")
	@AndroidFindBy (accessibility ="GET STARTED")
	public MobileElement GetStartedButton;

	@iOSXCUITFindBy (accessibility = "Please enter your mobile number")
	@AndroidFindBy (xpath ="//android.widget.EditText[@text='Mobile number']")
	public MobileElement mobileNumber;

		
	@iOSXCUITFindBy (accessibility = "LOGIN")
	@AndroidFindBy (accessibility ="LOGIN")
	public MobileElement LoginButton;

	@iOSXCUITFindBy (accessibility = "Business name")
	@AndroidFindBy (xpath ="//android.widget.EditText[@text='Business name']")
	public MobileElement businessName;

	@iOSXCUITFindBy (accessibility = "User Name")
	@AndroidFindBy (xpath ="//android.widget.EditText[@text='User Name']")
	public MobileElement userName;

	@iOSXCUITFindBy (accessibility = "E-mail")
	@AndroidFindBy (xpath ="//android.widget.EditText[@text='E-mail']")
	public MobileElement email;

	@iOSXCUITFindBy (accessibility = "NEXT")
	@AndroidFindBy (accessibility ="NEXT")
	public MobileElement nextButton;

	@iOSXCUITFindBy (accessibility = "CONFIRM")
	@AndroidFindBy (accessibility ="CONFIRM")
	public MobileElement confirmButton;

	
	
	@iOSXCUITFindBy (className  = "I have a\nRestaurant")
	@AndroidFindBy (accessibility="I have a\nRestaurant")
	public MobileElement ResButton;

	@iOSXCUITFindBy (className  = "I am a\nSupplier")
	@AndroidFindBy (accessibility="I am a\nSupplier")
	public MobileElement SubButton;

	@iOSXCUITFindBy (className = "")
	@AndroidFindBy (className ="android.widget.ImageView")
	public List<MobileElement> invalidNumber;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="🇸🇦\n+966   ")
	public MobileElement CountryPicker;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="🇦🇪\nUnited Arab Emirates\n+971")
	public MobileElement UAE;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility = "Looks like this number is registered as a restaurant!")
	public MobileElement ooops;
	

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Chats")
	public MobileElement chatButton;
	//\nChats\nTab 1 of 4

}
