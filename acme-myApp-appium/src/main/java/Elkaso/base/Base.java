package Elkaso.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.MobileCapabilityType;

public class Base {
	protected static AndroidDriver<MobileElement> driver;
	protected FileInputStream inputStream;
	protected Properties prop;
	public static ExtentReports extent;
	public static ExtentTest logger;

	public Base() {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);

	}

	@BeforeSuite
	public void beforeSuite() {
		extent = new ExtentReports("Reports/index.html");
		extent.addSystemInfo("Author", "Wael");
		extent.addSystemInfo("App", "Elkaso-Android");

	}

	@AfterSuite
	public void afterSuite() {
		extent.flush();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		// driver.resetApp();
		driver.launchApp();
		logger = extent.startTest(method.getName());
	}

	@AfterMethod
	public void afterMethod(Method method, ITestResult result) {

		File image = driver.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(image, new File("Snapshots/" + method.getName() + ".jpg"));
		} catch (IOException e) {

			e.printStackTrace();
		}

		String imageFullPath = System.getProperty("user.dir") + File.separator + "Snapshots/" + method.getName()
				+ ".jpg";

		if (result.getStatus() == ITestResult.SUCCESS) {
			logger.log(LogStatus.PASS, "Test is passed because there in no errors");
			// logger.log(LogStatus.PASS, logger.addScreenCapture(imageFullPath));
		} else if (result.getStatus() == ITestResult.FAILURE) {
			logger.log(LogStatus.FAIL, "Test is failed");
			logger.log(LogStatus.FAIL, result.getThrowable());
			logger.log(LogStatus.FAIL, logger.addScreenCapture(imageFullPath));

		} else {
			logger.log(LogStatus.SKIP, "Test is Skipped");

		}

		driver.closeApp();
	}

	@Parameters({ "deviceName", "platformName", "platformVersion" })
	@BeforeClass
	public void beforeClass(String deviceName, String platformName, String platformVersion) throws Exception {

		File propFile = new File("src/main/resources/config/config.properties");
		inputStream = new FileInputStream(propFile);
		prop = new Properties();
		prop.load(inputStream);

		if (platformName.equalsIgnoreCase("Android")) {

			File androidApp = new File(prop.getProperty("androidAppPath"));
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
			caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, prop.getProperty("androidAutomationName"));
			caps.setCapability(MobileCapabilityType.APP, androidApp.getAbsolutePath());
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("appWaitForLaunch", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("noReset", true);

			driver = new AndroidDriver<MobileElement>(new URL(prop.getProperty("appiumServerLink")), caps);

		} else if (platformName.equalsIgnoreCase("IOS")) {

			File iosApp = new File(prop.getProperty("iosAppPath"));
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
			caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, prop.getProperty("iosAutomationName"));
			caps.setCapability(MobileCapabilityType.APP, iosApp.getAbsolutePath());
			caps.setCapability(MobileCapabilityType.UDID, "00008020-00034D802ED3002E");
			caps.setCapability("bundleId", "ae.alpha-apps.Elkaso.beta");
			caps.setCapability("simpleIsVisibleCheck", true);
			caps.setCapability("useJSONSource", true);
			caps.setCapability("xcodeOrgId", "2AMMB6P52D");
			caps.setCapability("xcodeSigningId", "iPhone Developer");
			caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("showIOSLog", true);
			caps.setCapability("autoAcceptAlerts", true);
			caps.setCapability("useNewWDA", true);
			caps.setCapability("noReset", true);
			caps.setCapability("includeScreenInfoInSession", false);

			driver = new AndroidDriver<MobileElement>(new URL(prop.getProperty("appiumServerLink")), caps);

		}

		driver.manage().timeouts().implicitlyWait(32, TimeUnit.SECONDS);

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
