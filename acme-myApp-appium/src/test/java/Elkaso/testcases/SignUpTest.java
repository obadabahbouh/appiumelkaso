package Elkaso.testcases;


import org.testng.Assert;
import org.testng.annotations.Test;

import Actions.Action;
import Elkaso.screens.*;

public class SignUpTest extends Action {
	
	LoginScreen loginScreen; 
	PinScreen pinScreen ;
	
	@Test(priority=1,enabled = true)
	public void valid_restaurant_registration() throws InterruptedException {
		loginScreen = new LoginScreen();
		pinScreen = new PinScreen();
waitForElementToLoad(driver, loginScreen.GetStartedButton);
		click(loginScreen.GetStartedButton);
		click(loginScreen.mobileNumber);
		EnterValue("555999131");
		click(loginScreen.LoginButton);
	
		waitForElementToLoad(driver,pinScreen.verifyButton);
		EnterValue("1234");
		click(pinScreen.verifyButton);
		click(pinScreen.Num1);
		click(pinScreen.Num2);
		click(pinScreen.Num3);
		click(pinScreen.Num4);
		waitforseconds(20);
		click(pinScreen.Num1);
		click(pinScreen.Num2);
		click(pinScreen.Num3);
		click(pinScreen.Num4);
		waitforseconds(20);
		click(loginScreen.ResButton);
		click(loginScreen.LoginButton);
		waitForElementToLoad(driver, loginScreen.businessName);
		click(loginScreen.businessName);
		EnterValue("Gemini6");
		click(loginScreen.userName);
		EnterValue("Malek6");
		click(loginScreen.email);
		EnterValue("Gemini6@test.test");
		click(loginScreen.nextButton);
		backButton();
		waitForElementToLoad(driver,loginScreen.confirmButton);
		click(loginScreen.confirmButton);
		waitForElementToLoad(driver,loginScreen.confirmButton);
		click(loginScreen.confirmButton);
		waitForElementToLoad(driver,loginScreen.chatButton);
       final String text =loginScreen.chatButton.getAttribute("content-desc").toString();
		Assert.assertTrue(text.contains("Chats"));		
	}
	
	@Test(priority=2,enabled=false)
	public void userLoginAsResturantWithInvalidNumber() throws InterruptedException {
		loginScreen= new LoginScreen();
		pinScreen = new PinScreen();
		click(loginScreen.GetStartedButton);
		click(loginScreen.ResButton);
	
			click(loginScreen.LoginButton);
			//hideKeyboard();
			click(loginScreen.CountryPicker);
			click(loginScreen.UAE);
			waitforseconds(30);
			//hideKeyboard();
			//backButton();
			EnterValue("2211");
			
			click(loginScreen.LoginButton);
			waitforseconds(30);
		final String text =loginScreen.invalidNumber.stream().findFirst().get().getAttribute("content-desc").toString();
	    Assert.assertTrue(text.contains("Please check your phone number"));		

	
	}
	@Test(priority=3,enabled=false)
	public void userLoginAsSupplierWithValidNumber() throws InterruptedException {
		loginScreen= new LoginScreen();
		pinScreen = new PinScreen();
		
		click(loginScreen.GetStartedButton);
		click(loginScreen.SubButton);
		click(loginScreen.LoginButton);
		//hideKeyboard();
		click(loginScreen.CountryPicker);
		click(loginScreen.UAE);
		waitforseconds(30);
		//hideKeyboard();
		EnterValue("544332233");
		
		click(loginScreen.LoginButton);
		click(pinScreen.Num1);
		click(pinScreen.Num2);
		click(pinScreen.Num3);
		click(pinScreen.Num4);
	
       final String text =loginScreen.chatButton.getAttribute("content-desc").toString();
		Assert.assertTrue(text.contains("Chats"));		
	}
	
	@Test(priority=4,enabled=false)
	public void userLoginAsSupplierWithResturantNumber() throws InterruptedException {
		loginScreen= new LoginScreen();
		pinScreen = new PinScreen();
		
		click(loginScreen.GetStartedButton);
		//  MobileElement SupButton = loginScreen.ViewButton.stream().filter(x->x.getAttribute("content-desc").contains("Supplier")).findFirst().get();
		//  click(SupButton);
		click(loginScreen.SubButton);
		click(loginScreen.LoginButton);
		//hideKeyboard();
		click(loginScreen.CountryPicker);
		click(loginScreen.UAE);
		waitforseconds(30);
		//hideKeyboard();
		EnterValue("544332211");
		click(loginScreen.LoginButton);
	
	
       final String text =loginScreen.ooops.getAttribute("content-desc").toString();
		Assert.assertTrue(text.contains("registered as a restaurant!"));		
	}
	
}
